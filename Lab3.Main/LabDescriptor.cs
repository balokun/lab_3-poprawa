﻿using System;
using Lab3.Contract;
namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(Interface1);
        public static Type I2 = typeof(Interface2);
        public static Type I3 = typeof(Interface3);

        public static Type Component = typeof(PanelKontrolny);

        public static GetInstance GetInstanceOfI1 = (component) => component;
        public static GetInstance GetInstanceOfI2 = (component) => component;
        public static GetInstance GetInstanceOfI3 = (component) => component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Ext);
        public static Type MixinFor = typeof(PanelKontrolny);

        #endregion
    }
}
